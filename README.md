## Objetivos:

- Desenvolver um “Carrinho de compras” usando uma API.
- Listar os produtos, exibir o total da compra e exibir uma mensagem informando se o pedido possui frete grátis.

## **Prazo:**

- 3 dias corridos.

## **Requisitos mínimos:**

- Listar os produtos provenientes da API.
- Decidir a forma como deverá lidar com os diferentes tipos de JSON (com React Router ou da outra forma que preferir)
- Os produtos devem ter imagem, nome e preço.
- Exibir ao fim da lista o valor total de todos os produtos.
- Exibir o texto de frete grátis dependendo do valor do carrinho.
- O texto de frete grátis deverá aparecer apenas se o valor for acima de **R$ 10,00.**
- Seguir o layout. Mas é possível adicionar outros elementos como forma de melhoria se preferir.
- Usar Flex-Box CSS.
- Utilizar ReactJS (com NextJS, Create React App ou até mesmo GatsbyJS)
- Enviar o link do teste no Github.
- Hospedá-lo em algum local (Netlify CMS, Vercel, Heroku ou outra opção)

Atingindo os requisitos mínimos, você poderá explorar a API e o layout**. U**se sua criatividade para incrementar o seu teste, deste que esteja dentro do prazo.

<aside>
⚠️ Os links dos JSONs abaixo não devem ser usados para o desenvolvimento. É necessário baixar ambos e hospedá-los em um local de sua preferência ou mantê-los dentro do projeto. Eles são expirados diariamente, logo caso utilize-os no seu teste, o mesmo poderá não funcionar da forma esperada durante a correção, resultando em desclassificação.

</aside>