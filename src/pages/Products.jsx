import axios from "axios";
import { useState, useEffect } from "react";
import Cart from "../components/Cart";
import styles from "./app.module.scss";

export const UnderProducts = ({baseURL}) => {
  const [products, setProducts] = useState([]);
  const [total, setTotal] = useState(0);

  const handleloadProducts = async () => {
    if (!baseURL) {
      return
    }

    const response = await axios.get(`data/${baseURL}`);
    const { items, value } = response.data;
    let formatValue = (value / 100).toFixed(2);
    let formatProducts = [];

    items.map((item) => {
      formatProducts.push({
        imageUrl: item.imageUrl,
        name: item.name,
        price: (item.price / 100).toFixed(2),
        sellingPrice: (item.sellingPrice / 100).toFixed(2),
      });
    });

    setTotal(formatValue);
    setProducts(formatProducts);
  };

  useEffect(() => {
    handleloadProducts();
  }, [baseURL]);

  return (
    <div className={styles.App}>
      <h1 className={styles.title}>Meu carrinho</h1>
      <Cart products={products} total={total} />
      <button>Finalizar compra</button>
    </div>
  );
}