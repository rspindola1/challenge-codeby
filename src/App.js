import { useState, useEffect } from "react";
import styles from "./app.module.scss";
import { UnderProducts } from "./pages/Products";
import { UNDER_URL, ABOVE_URL } from "./utils/constants";
import Switch from "./components/Switch";

function App() {
  const [url, setUrl] = useState();
  const [value, setValue] = useState(false);

  useEffect(() => {
    if (value) {
      setUrl(ABOVE_URL);
    } else {
      setUrl(UNDER_URL);
    }
  }, [value]);

  return (
    <div className={styles.App}>
      <Switch isOn={value} handleToggle={() => setValue(!value)} />
      <UnderProducts baseURL={url} />
    </div>
  );
}

export default App;
