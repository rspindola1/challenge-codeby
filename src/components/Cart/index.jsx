import styles from "./style.module.scss";

const Cart = ({ products, total }) => {
  return (
    <div className={styles.cart}>
      <div className={styles.cartItems}>
        {products &&
          products.map((product, index) => (
            <div className={styles.item} key={index}>
              <div className={styles.imgProduct}>
                <img src={product.imageUrl} />
              </div>
              <div className={styles.productInfo}>
                <p className={styles.nameProduct}>{product.name}</p>
                <p className={styles.oldPrice}>R$ {product.price}</p>
                <p className={styles.price}>R$ {product.sellingPrice}</p>
              </div>
            </div>
          ))}
      </div>
      <div className={styles.checkout}>
        <div className={styles.result}>
          <p>Total</p>
          <p>R$ {total}</p>
        </div>
        {total * 100 >= 1000 && (
          <div className={styles.discount}>
            <p>Parabéns, sua compra tem frete grátis!</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default Cart;
