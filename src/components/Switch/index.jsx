import React from "react";
import styles from "./style.module.scss";

const Switch = ({ isOn, handleToggle }) => {
  return (
    <>
      <input
        checked={isOn}
        onChange={handleToggle}
        className={styles.reactSwitchCheckbox}
        id={`reactSwitchNew`}
        type="checkbox"
      />
      <label className={styles.reactSwitchLabel} htmlFor={`reactSwitchNew`}>
        <span className={styles.reactSwitchButton} />
      </label>
    </>
  );
};

export default Switch;
